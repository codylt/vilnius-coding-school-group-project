<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
	<title>Apie mus</title>
</head>
<body>

	<?php include 'header.php' ?>
	<h2 class="menu-header">Mūsų vertybės</h2>
<div class="faq-vertical">
<div>
	<ul class="collapsible expandable drop-wrapper">
	    <li>
	      	<div class="collapsible-header pirmasCollapsible"><i class="material-icons"></i>Vietinė produkcija</div>
	      	<div class="collapsible-body">
	      		<span class="center-span">
KAZIMIRO PICOS veiklos sėkmė slypi  tvirtuose ryšiuose su vietiniais ūkininkais. Iš patikimų rankų KAZIMIRO PICOS įsigyja tuos produktus, kurių neaugina pats, bet yra tikras dėl jų kokybės. Šis bendradarbiavimas  ne tik kuria gražius kaimyniškus santykius ir sutvirtina bendruomenę, bet ir užtikrina, kad picerijos klientas gali rinktis iš gausybės sezoninių, vadinasi, ir vertingiausių maistiniu požiūriu produktų.
	     		</span>
	  		</div>	
	    </li>
	    <li>
	      <div class="collapsible-header antrasCollapsible"><i class="material-icons"></i>Kokybė</div>
	      <div class="collapsible-body">
	      		<span class="center-span">
	      			Daržovės ir vaisiai, mėsa ir pienas, kurie pasiekia Kazimiro piceriją yra švieži, nes užauginti čia pat, jie, kitaip nei atvežtiniai produktai, nėra transportuojami  dienų dienas, todėl yra išsaugoję daugiau maistinių savybių. Jie sveikesni ir gardesni, nes jų nereikia nupurkšti tam, kad ilgiau išlaikytų prekinę išvaizdą. Be to, vietinė produkcija pasižymi skonių ir rūšių įvairove, ji turi savo istoriją. To neturi prekybos centruose siūlomos daržovės, vaisiai, mėsos ar pieno produktai. 
	     		</span>
	  		</div>
	    </li>
	    <li>
	      <div class="collapsible-header treciasCollapsible"><i class="material-icons"></i>Gamtos apsauga</div>
	      <div class="collapsible-body">
	      		<span class="center-span">
	      			Vartojant vietinę produkciją sudaromos sąlygas vietiniams ūkininkams efektyviai išnaudoti žemės plotus, kuriamos papildomos darbo vietos ir remiama Lietuvos  ekonomika. Be to, vietinės produkcijas vartojimas turi mažiau poveikio gamtai. Kuo ilgiau produktai keliauja iki prekybos centro lentynų, tuo daugiau teršiama aplinka, tokią produkciją reikia specialiai perdirbti, apdoroti, supakuoti. Visa tai daro neigiamą įtaką gamtai. Taigi, draugiškiausi aplinkai yra vietinės produkcijos kūrėjai ir jos vartotojai. 
	     		</span>
	  		</div>
	    </li>
  	</ul>
</div>
</div>

    <?php include 'footer.php' ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script type="text/javascript" src="scripts/scripts.js"></script>
</body>
</html>