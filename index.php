<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
  <link href="https://fonts.googleapis.com/css?family=Kavivanar" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Patrick+Hand+SC" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Allan" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
	<title>Kazimiro picos</title>
</head>
<body>
	<?php include 'header.php' ?>

<div class="carousel-wrapper">
  <div class="carousel carousel-slider center">
    <div class="carousel-fixed-item center">
    </div>
    <div style="background-image: url(images/pica1.jpg)" class="carousel-item red white-text" href="meniu.php">
      <h2 class="front-page-pica">Geriausios picos mieste!</h2>
    </div>
    <div style="background-image: url(images/pica2.jpg)"  class="carousel-item amber white-text" href="#meniu.php">
      <h2>Užsisakykite nemokamai!</h2>
    </div>
    <div style="background-image: url(images/pica3.jpg)"  class="carousel-item green white-text" href="meniu.php">
      <h2>Kokybiškiausi produktai!</h2>
    </div>
  </div>
</div>


<!-- Parralax -->
<div class="parallax-container">
      <div class="parallax"><img src="images/parralax.jpg"></div>
    </div>
<!-- Papildomas paragrafas po karusele -->

<p class="second-row pavadinimas menu-header"> Mūsų darbuotojai </p>
<div class="about-us-wrapper">
<div class="row">
      <div class="col l4">
        <div class="bio">
          <li>
            <img class="face" src="https://s3.theoryandpractice.ru/uploads/image_avatar/000/066/629/image/9fbdf59772.png"></li>
            <br>
            <p class="name"><b>Dennis Kim</b></p>
            <br>
            <p class="information">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>




      <div class="col l4">
        <div class="bio">
        <li>
          <img class="face" src="http://marketing.by/upload/medialibrary/4a2/circle%20(10).png"></li>
          <br>
            <p class="name"><b>John McCoy</b></p>
            <br>
            <p class="information">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>




      <div class="col l4">
        <div class="bio">
          <li>
          <img class="face" src="http://byebyeballet.ru/wp-content/uploads/2013/02/circle-4.png"></li>
          <br>
            <p class="name"><b>Emily Porter</b></p>
            <br>
            <p class="information">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        </div>
      </div>



</div>
</div>
      <?php include 'footer.php' ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script type="text/javascript" src="scripts/scripts.js"></script>
</body>
</html>