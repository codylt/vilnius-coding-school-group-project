<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
	<title>Apie mus</title>
</head>
<body style="background-color:#fff;">

	<?php include 'header.php' ?>
	

	<h2 class="About_us_h2 menu-header">Apie mus</h2>

	
	<div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col l4"><a href="#test1">Tomas</a></li>
        <li class="tab col l4 "><a href="#test3">Raimundas</a></li>
        <li class="tab col l4"><a href="#test4">Evelina</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12"> 
    	<img class="col l3" class="face" src="images/tomas.png">
    	<p class="about-us-p col l9">Piceriją, kurioje jau daugiau nei dešimtmetį puoselėjamos unikalaus ir išskirtinio skonio tradicijos, piceriją, kurioje visada esi laukiamas. Mums, kaip ir Tau, yra svarbu, kad maistas būtų geras ir kokybiškas, todėl patyrę mūsų virtuvės šefai stengiasi pasiūlyti įvairiausių skonių picas ir kitus patiekalus, pagamintus tik iš aukščiausios kokybės produktų ir sukurtus pagal naujausias mitybos tendencijas. Begalinė skonių ir kvapų jūra maloniai nustebins ir pradžiugins net ir išrankiausio skonio gurmanus, o draugiškos kainos - pakels nuotaiką visai dienai.
Šiuolaikiška ir moderni aplinka leis pasijusti taip, lyg pietautum Manheteno centre, o malonus ir šiltas aptarnavimas vilios sugrįžti dar ne kartą. KAZIMIRO PIZZA ateitį kuria šiandien, todėl mes esame viena populiariausių picerijų visoje Lietuvoje.
Kiekvienais metais mus aplanko daugiau nei 6 milijonai klientų, tačiau mūsų tikslas – ne nuolatos augantis klientų skaičius, mūsų tikslas, kad kiekvienas, užsukęs pas mus, jaustųsi lyg grįžęs į namus, skaniai pavalgytų, maloniai praleistų laiką ir išeitų plačiai šypsodamasis. Tik Tu ir Tavo poreikiai mums yra svarbiausi, todėl nesiliaujame tobulėti ir stebinti maloniomis ir labai gardžiomis naujienomis bei patraukliais pasiūlymais.</p>
    </div>


    <div id="test3" class="col s12">
    	<img class="col l3" class="face" src="images/raimundas.png">
    	<p class="about-us-p col l9">
Šiuolaikiška ir moderni aplinka leis pasijusti taip, lyg pietautum Manheteno centre, o malonus ir šiltas aptarnavimas vilios sugrįžti dar ne kartą. KAZIMIRO PIZZA ateitį kuria šiandien, todėl mes esame viena populiariausių picerijų visoje Lietuvoje.
Kiekvienais metais mus aplanko daugiau nei 6 milijonai klientų, tačiau mūsų tikslas – ne nuolatos augantis klientų skaičius, mūsų tikslas, kad kiekvienas, užsukęs pas mus, jaustųsi lyg grįžęs į namus, skaniai pavalgytų, maloniai praleistų laiką ir išeitų plačiai šypsodamasis. Tik Tu ir Tavo poreikiai mums yra svarbiausi, todėl nesiliaujame tobulėti ir stebinti maloniomis ir labai gardžiomis naujienomis bei patraukliais pasiūlymais.</p>




    </div>
    <div id="test4" class="col s12">
    	<img class="col l3" class="face" src="images/evelina.png">
    	<p class="about-us-p col l9">Piceriją, kurioje jau daugiau nei dešimtmetį puoselėjamos unikalaus ir išskirtinio skonio tradicijos, piceriją, kurioje visada esi laukiamas. Mums, kaip ir Tau, yra svarbu, kad maistas būtų geras ir kokybiškas, todėl patyrę mūsų virtuvės šefai stengiasi pasiūlyti įvairiausių skonių picas ir kitus patiekalus, pagamintus tik iš aukščiausios kokybės produktų ir sukurtus pagal naujausias mitybos tendencijas. Begalinė skonių ir kvapų jūra maloniai nustebins ir pradžiugins net ir išrankiausio skonio gurmanus, o draugiškos kainos - pakels nuotaiką visai dienai.
</p>





    </div>
  </div>


		<!-- zemelapis -->
		
	    



	    <!-- kontaktai -->
	      <div class="row center-me-about">
<div class="col l10 contacts-about">
<div class="map-wrapper">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d147554.67569876468!2d25.112849926117203!3d54.70009023979885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd93fb5c6408f5%3A0x400d18c70e9dc40!2sVilnius!5e0!3m2!1slt!2slt!4v1527524810868" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>
	    <div class="col l2 contacts-about">
	    	<p>
	    		Name: Kazimiro picerija<br>
	    		Address: Ulonų g. 5<br>
	    		City: Vilnius<br>
	    		Country: Lietuva<br>
	    		Mobile: +3706222**29<br>
	    		Landline: 8523334<br>
	    		Email: kazimiropicos@picerija.lt<br>
	    	</p>
	    </div>
  </div>

<?php include 'footer.php' ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script type="text/javascript" src="scripts/scripts.js"></script>
</body>
</html>