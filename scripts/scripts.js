document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);

    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems);

    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems);

    var elem = document.querySelector('.collapsible.expandable');
    var instance = M.Collapsible.init(elem, {
    accordion: false              //  kad neuzsidarytu atidarytas klausimas
    });
});

var instance = M.Carousel.init({
    fullWidth: true,
    indicators: true
  });

$( document ).ready(function(){
	$(".dropdown-trigger").dropdown({ hover: true });
    $(".dropdown-trigger1").dropdown({ hover: false });

    $('.tabs').tabs();

    var elems = document.querySelector('#modal1.modal');
     instanceorder = M.Modal.init(elems);

    var elems = document.querySelector('#modal2.modal');
     instancebye = M.Modal.init(elems);

    var elems = document.querySelectorAll('select');
    var selectinstances = M.FormSelect.init(elems);

    $("#my_form").submit(function(event){
    event.preventDefault(); //prevent default action 
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    var picos_id = selectinstances[0].getSelectedValues().join(",");
    form_data= form_data + "&picos=" + picos_id;

    console.log(form_data);
    $.ajax({
        url : post_url,
        type: request_method,
        data : form_data
    }).done(function(response){
        console.log(response);
    });

    var modal2content = $('#modal2.modal').html();
    $('#modal1.modal').html(modal2content);

});
});

