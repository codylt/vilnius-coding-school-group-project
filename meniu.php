<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
	<title>Meniu</title>
</head>
<body style="background-color:#fff;">

	<?php include 'header.php' ?>

	<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname	="uzsakymas";
	// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);

		mysqli_set_charset($conn, "utf8"); $sql = "SELECT ID, Img, Name, Price, description FROM picu_sarasas";

	$result = mysqli_query($conn, $sql);
//------------- atspausdina visa sarasa is db ----------
		if (mysqli_num_rows($result) > 0) {

		    while($row = mysqli_fetch_assoc($result)) {

		   	echo "
				<div class='menu-wrapper'>
					<ul class='collection'>
					    <li class='collection-item avatar'>
					      <img src=" . $row["Img"] . " class='circle'>
					      <span class='title'>" . $row["Name"] . "</span>
						      <p>
						      	" . $row["description"] . "
						      </p>
					      <div class='price_button'>
					      	<a href='#!'>
					      		<p>" . $row["Price"] . "&euro;</p>
					      	</a>
					      	
					      	<a class=' modal-trigger' href='#modal1'>
					      		<i data-pica=" . $row["ID"] . " class=' add-pica material-icons'>add_box</i>
					      	</a>
					      </div>							
					    </li>
					</ul>
				</div>";
		    }
		}
	?>
<!-- ------------------------------------------ -->	
<!-- --------------- uzsakymo forma ----------- -->

  <div id="modal1" class="modal">
    <div class="modal-content">

      	<h4>Užsakymas:</h4>
		<div class="row">
		    <form class="col s12 " action="form.php" method="post" id="my_form">
			      <div class="row">
			        <div class="input-field col s6">
			          <input name="vardas" id="first_name" type="text" class="validate">
			          <label for="first_name">Vardas</label>
			        </div>

			        <div class="input-field col s6">
			          <input name="pavarde" id="last_name" type="text" class="validate">
			          <label for="last_name">Pavardė</label>
			        </div>
			      </div>

			      <div class="row">
			        <div class="input-field col s12">
			          <input name="adresas" id="adress" type="text" class="validate">
			          <label for="adress">Adresas</label>
			        </div>
			      </div>

			      <div class="row">
			        <div class="input-field col s12">
			          <input name="numeris" id="phoneNumber" type="text" class="validate">
			          <label for="phoneNumber">Telefono numeris</label>
			        </div>
			      </div>

			      <div class="row">
			        <div class="input-field col s12">
			          <input name="emailas" id="email" type="email" class="validate">
			          <label for="email">Elektroninis paštas</label>
			        </div>
			      </div>

				<div class='input-field col s12'>
					<select multiple name="picos_id">
					    <option  value="" disabled selected>Pasirinkite picas:</option>
							<?php
							$result = mysqli_query($conn, $sql);
								if (mysqli_num_rows($result) > 0) {
								    while($row = mysqli_fetch_assoc($result)) {
								   	 echo "<option value=" . $row["ID"] . ">" . $row["Name"] . " " . $row["Price"] . "&euro;</option>";
								    }
								}
								mysqli_close($conn);
							?>
 					</select>
					<label>Picų pasirinkimas:</label>
				</div>

		      	<p>Padažų pasirinkimas (+ 0.5€/vnt):</p>

			    	<p>
				      	<label>
					        <input name="cesnakinis" type="checkbox" class="filled-in"/>
					        <span>Česnakinis</span>
				      	</label>
				      	<label>
					      	<input name="astrus" type="checkbox" class="filled-in"/>
					        <span>Aštrus</span>
				      	</label>
				      	<label>
					      	<input name="pikantiskas" type="checkbox" class="filled-in"/>
					        <span>Pikantiškas</span>
				      	</label>
			    	</p>

			    	<button class="btn">Užsakyti</button>
		    </form>
		</div>
    </div>
  </div>
<!-- ------------------------------------------ -->
<!-- -------- uzsakymo patvirtinimo modal ----- -->

  <div id="modal2" class="modal">
    <div class="modal-content">
      <h4>Jūsų užsakymas priimtas!</h4>
      <p>Ačiū, kad pasirinkote mus :) </p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Gerai</a>
    </div>
  </div>

<!-- ------------------------------------------ -->
    <?php include 'footer.php' ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script type="text/javascript" src="scripts/scripts.js"></script>
</body>
</html>